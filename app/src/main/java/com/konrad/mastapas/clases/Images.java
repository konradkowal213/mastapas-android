package com.konrad.mastapas.clases;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by konra on 21/05/2017.
 */

public class Images {
    private String ID_Imagen;
    private String base64_img;

    public Images(JSONObject response){

        try {
            ID_Imagen=response.getString("ID_Imagen");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            base64_img=response.getString("base64_img");
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public String getID_Imagen() {
        return ID_Imagen;
    }

    public void setID_Imagen(String ID_Imagen) {
        this.ID_Imagen = ID_Imagen;
    }

    public String getBase64_img() {
        return base64_img;
    }

    public void setBase64_img(String base64_img) {
        this.base64_img = base64_img;
    }
}
