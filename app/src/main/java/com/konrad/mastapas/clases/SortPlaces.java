package com.konrad.mastapas.clases;

import com.google.android.gms.maps.model.LatLng;

import java.util.Comparator;

/**
 * Created by konra on 27/05/2017.
 */

public class SortPlaces implements Comparator<Tapa> {
    LatLng currentLoc;

    public SortPlaces(LatLng current){
        currentLoc = current;
    }
    @Override
    public int compare(final Tapa place1,final Tapa place2) {
        double lat1 = Double.parseDouble(place1.getLatitud());
        double lon1 = Double.parseDouble(place1.getLongitud());
        double lat2 = Double.parseDouble(place2.getLatitud());
        double lon2 = Double.parseDouble(place2.getLongitud());

        double distanceToPlace1 = distance(currentLoc.latitude, currentLoc.longitude, lat1, lon1);
        double distanceToPlace2 = distance(currentLoc.latitude, currentLoc.longitude, lat2, lon2);
        return (int) (distanceToPlace1 - distanceToPlace2);
    }

    public double distance(double fromLat, double fromLon, double toLat, double toLon) {
        double radius = 6378137;   // approximate Earth radius, *in meters*
        double deltaLat = toLat - fromLat;
        double deltaLon = toLon - fromLon;
        double angle = 2 * Math.asin( Math.sqrt(
                Math.pow(Math.sin(deltaLat/2), 2) +
                        Math.cos(fromLat) * Math.cos(toLat) *
                                Math.pow(Math.sin(deltaLon/2), 2) ) );
        return radius * angle;
    }

}
