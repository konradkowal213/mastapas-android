package com.konrad.mastapas.clases;

import android.content.Context;
import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.konrad.mastapas.adapters.listTapasNearbyAdapter;
import com.konrad.mastapas.dataParser.DataParser;
import com.konrad.mastapas.fragments.NewLocalAndCommentFragment;
import com.konrad.mastapas.urlConnections.UrlConnection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.konrad.mastapas.ApplicationConfig.tapasCercania;

public class GetNearbyTapasData extends AsyncTask<Object, String, String> {

    String googlePlacesData;
    String url;
    ArrayList<Tapa> arrayTapas=new ArrayList<>();
    Location location;
    ListView lista;
    Context context;
    TextView texto;
    NewLocalAndCommentFragment fragment;

    @Override
    protected String doInBackground(Object... params) {
        try {
            Log.d("GetNearbyBanksData", "doInBackground entered");
            url = (String) params[0];
            location = (Location) params[1];
            lista = (ListView) params[2];
            context=(Context) params[3];
            texto = (TextView) params [4];
            fragment = (NewLocalAndCommentFragment) params[5];
            UrlConnection urlConnection = new UrlConnection();
            googlePlacesData = urlConnection.readUrl(url);
            Log.d("GooglePlacesReadTask", "doInBackground Exit");
        } catch (Exception e) {
            Log.d("GooglePlacesReadTask", e.toString());
        }
        return googlePlacesData;
    }

    @Override
    protected void onPostExecute(String result) {
        Log.d("GooglePlacesReadTask", "onPostExecute Entered");
        List<HashMap<String, String>> nearbyPlacesList = null;
        DataParser dataParser = new DataParser();
        nearbyPlacesList =  dataParser.parse(result);
        //ShowNearbyPlaces(nearbyPlacesList);
        Tapa p;
        for(int f=0;f<nearbyPlacesList.size();f++){
            p=new Tapa(nearbyPlacesList.get(f).get("place_name"),nearbyPlacesList.get(f).get("street"),nearbyPlacesList.get(f).get("lat"),nearbyPlacesList.get(f).get("lng"));
            arrayTapas.add(p);
        }
        SetNearbyPlaces(arrayTapas);
        tapasCercania=arrayTapas;
    }




    private void SetNearbyPlaces(ArrayList<Tapa> array) {
        if(fragment.isVisible()) {
            lista.setVisibility(View.VISIBLE);
            listTapasNearbyAdapter adapter = new listTapasNearbyAdapter(context, array);
            lista.setAdapter(adapter);
            texto.setText("Lista de sitios ordenados por cercanía");
        }
    }
}
