package com.konrad.mastapas.clases;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by konra on 10/05/2017.
 */

public class Tapa {
    private String idTapa;
    private String categoria;
    private String nombre;
    private String calle;
    private String latitud;
    private String longitud;
    private String puntuacionMedia;
    private String desc;
    private String precio;
    private String fecha_publicacion;
    private String puntuacion;

    public Tapa(JSONArray response){
        try {
            this.idTapa=response.getJSONObject(0).getString("ID_tapa");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            this.categoria=response.getJSONObject(0).getString("Categoria");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            this.nombre=response.getJSONObject(0).getString("Nombre");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            this.calle=response.getJSONObject(0).getString("Calle");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            this.latitud=response.getJSONObject(0).getString("Latitud");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            this.longitud=response.getJSONObject(0).getString("Longitud");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            this.puntuacion=response.getJSONObject(0).getString("Puntuacion");
        } catch (JSONException e) {
            e.printStackTrace();
        } try {
            this.desc=response.getJSONObject(0).getString("Descripcion");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            this.precio=response.getJSONObject(0).getString("Precio");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            this.fecha_publicacion=response.getJSONObject(0).getString("Fecha_publicacion");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            this.puntuacion=response.getJSONObject(0).getString("Puntuacion media");
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public Tapa(JSONObject response){
        try {
            this.idTapa=response.getString("ID_tapa");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            this.categoria=response.getString("Categoria");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            this.nombre=response.getString("Nombre");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            this.calle=response.getString("Calle");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            this.latitud=response.getString("Latitud");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            this.longitud=response.getString("Longitud");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            this.puntuacion=response.getString("Puntuacion");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            this.desc=response.getString("Descripcion");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            this.precio=response.getString("Precio");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            this.fecha_publicacion=response.getString("Fecha_publicacion");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            this.puntuacionMedia=response.getString("Puntuacion media");
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public Tapa(String nombre, String calle, String lat, String lng){
        this.nombre=nombre;
        this.calle=calle;
        this.latitud=lat;
        this.longitud=lng;
    }

    public String getIdTapa() {
        return idTapa;
    }

    public void setIdTapa(String idTapa) {
        this.idTapa = idTapa;
    }

    public String getPuntuacion() {
        return puntuacion;
    }

    public void setPuntuacion(String puntuacion) {
        this.puntuacion = puntuacion;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getPuntuacionMedia() {
        return puntuacionMedia;
    }

    public void setPuntuacionMedia(String puntuacionMedia) {
        this.puntuacionMedia = puntuacionMedia;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getFecha_publicacion() {
        return fecha_publicacion;
    }

    public void setFecha_publicacion(String fecha_publicacion) {
        this.fecha_publicacion = fecha_publicacion;
    }
}
