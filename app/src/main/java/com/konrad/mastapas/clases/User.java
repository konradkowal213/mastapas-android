package com.konrad.mastapas.clases;

import android.provider.BaseColumns;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by konra on 15/05/2017.
 */

public class User {
    private String id_user;
    private String nick;
    private String mail;
    private String pass;//la que se introduce en el login
    private String api_Token;
    private String token_expiration;


    public static abstract class UserEntry implements BaseColumns {
        public static final String TABLE_NAME ="user";
        public static final String ID = "id_user";
        public static final String NICK = "nick";
        public static final String MAIL = "mail";
        public static final String PASS = "pass";
        public static final String API_TOKEN = "apiToken";
    }

    public User(JSONArray response){
        try {
            this.nick=response.getJSONObject(0).getString("Nick");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public User(){

    }

    public User(String id_user,String nick, String mail, String pass, String api_Token, String token_expiration){
        this.id_user=id_user;
        this.nick=nick;
        this.mail=mail;
        this.pass=pass;
        this.api_Token=api_Token;
        this.token_expiration=token_expiration;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getApi_Token() {
        return api_Token;
    }

    public void setApi_Token(String api_Token) {
        this.api_Token = api_Token;
    }

    public String getToken_expiration() {
        return token_expiration;
    }

    public void setToken_expiration(String token_expiration) {
        this.token_expiration = token_expiration;
    }

}
