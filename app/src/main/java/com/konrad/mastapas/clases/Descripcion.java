package com.konrad.mastapas.clases;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by konra on 21/05/2017.
 */



public class Descripcion {
    private String ID_Descripcion;
    private String descripcion;
    private String aprobado;
    private String precio;
    private String fecha_publicacion;
    private String puntuacion;
    private User usuario;
    private Tapa tapa;
    private Images imagen;
    private ArrayList<Images> arrayImagenes;

    public Descripcion(JSONObject response){
        try {
            this.ID_Descripcion=response.getString("ID_descripcion");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            this.aprobado=response.getString("Confirmado");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            this.descripcion=response.getString("Descripcion");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            this.precio=response.getString("Precio");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            this.fecha_publicacion=response.getString("Fecha_publicacion");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            this.puntuacion=response.getString("Puntuacion");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            this.usuario=new User(response.getJSONArray("Usuario"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            this.tapa=new Tapa(response.getJSONArray("Tapa"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            for(int f=0;f<response.getJSONArray("Imagenes").length();f++){
                imagen=new Images(response.getJSONArray("Imagenes").getJSONObject(f));
                arrayImagenes=new ArrayList<Images>();
                arrayImagenes.add(imagen);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }




    }

    public String getAprobado() {
        return aprobado;
    }

    public void setAprobado(String aprobado) {
        this.aprobado = aprobado;
    }

    public String getID_Descripcion() {
        return ID_Descripcion;
    }

    public void setID_Descripcion(String ID_Descripcion) {
        this.ID_Descripcion = ID_Descripcion;
    }

    public Images getImagen() {
        return imagen;
    }

    public void setImagen(Images imagen) {
        this.imagen = imagen;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getFecha_publicacion() {
        return fecha_publicacion;
    }

    public void setFecha_publicacion(String fecha_publicacion) {
        this.fecha_publicacion = fecha_publicacion;
    }

    public String getPuntuacion() {
        return puntuacion;
    }

    public void setPuntuacion(String puntuacion) {
        this.puntuacion = puntuacion;
    }

    public User getUsuario() {
        return usuario;
    }

    public void setUsuario(User usuario) {
        this.usuario = usuario;
    }

    public Tapa getTapa() {
        return tapa;
    }

    public void setTapa(Tapa tapa) {
        this.tapa = tapa;
    }

    public Images getImagenes() {
        return imagen;
    }

    public void setImagenes(Images imagenes) {
        this.imagen = imagenes;
    }

    public ArrayList<Images> getArrayImagenes() {
        return arrayImagenes;
    }

    public void setArrayImagenes(ArrayList<Images> arrayImagenes) {
        this.arrayImagenes = arrayImagenes;
    }
}
