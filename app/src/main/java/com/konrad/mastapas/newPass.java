package com.konrad.mastapas;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class newPass extends AppCompatActivity {

    TextView name, mail, pass1,pass2, nick;
    Button btn_success;
    String emailExpression = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_pass);
        mail=(TextView) findViewById(R.id.mail);
        mail.setTextColor(Color.WHITE);
        progressDialog = new ProgressDialog(newPass.this, R.style.Theme_AppCompat_DayNight_Dialog);
        nick=(TextView) findViewById(R.id.nick);
        nick.setTextColor(Color.WHITE);
        btn_success=(Button) findViewById(R.id.btn_success);
        btn_success.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog.setIndeterminate(true);
                progressDialog.setCancelable(false);
                progressDialog.setMessage("Restableciendo contraseña...");
                if(validar()==true){
                    progressDialog.show();

                    sendData();
                }
            }
        });
    }

    private boolean validar(){


        if(mail.getText().toString().matches(emailExpression)){
        }else{
            mail.setError("No es un correo válido");
            return false;
        }
        if(nick.length()<5){
            nick.setError("El nick debe contener al menos 5 carácteres");
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        Intent newAccount = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(newAccount);
        finish();
    }

    private void sendData(){


        Map<String, String> params = new HashMap();
        params.put("nick", String.valueOf(nick.getText()));
        params.put("correo", String.valueOf(mail.getText()));

        JSONObject parameters = null;

        parameters = new JSONObject(params);



        String url = ApplicationConfig.URL + "lostPassword";

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
                new AlertDialog.Builder(newPass.this)
                        .setTitle("Contraseña restablecida")
                        .setMessage("Te hemos enviado una nueva contraseña al correo, si no la ves, comprueba la carpeta de SPAM")
                        .setCancelable(false)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                Intent newAccount = new Intent(getApplicationContext(), LoginActivity.class);
                                startActivity(newAccount);
                                finish();
                            }
                        })
                        .show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                //501 ya existe nick o usuario ,500 error interno del servidor
                if (error.networkResponse.statusCode == 500) {
                    new AlertDialog.Builder(newPass.this)
                            .setTitle("Ups!")
                            .setCancelable(false)
                            .setMessage("Error interno del servidor, intentelo mas tarde")
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .show();
                }else{
                    new AlertDialog.Builder(newPass.this)
                            .setTitle("Ups!")
                            .setCancelable(false)
                            .setMessage("Error interno del servidor, intentelo mas tarde")
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .show();
                }
            }
        });

        Volley.newRequestQueue(this).add(jsonRequest);

    }

}
