package com.konrad.mastapas;

import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import com.google.android.gms.maps.model.LatLng;
import com.konrad.mastapas.clases.Descripcion;
import com.konrad.mastapas.clases.Tapa;
import com.konrad.mastapas.clases.User;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

/**
 * Created by konra on 15/05/2017.
 */

public class ApplicationConfig extends Application{

    public static User currentUser=new User();
    private static Context mContext;
    public static String m;
    public static String URLocal="http://192.168.1.134/plantillamastapas/api/";
    public static String URL="http://mastapas.es/api/";
    public static ArrayList<Tapa> tapasCercania=new ArrayList<Tapa>();
    public static ArrayList<Tapa> tapas=new ArrayList<Tapa>();
    public static boolean misOpiniones=false;
    public static ArrayList<Descripcion> descripcions=new ArrayList<Descripcion>();
    public static boolean comentariosAbiertos=false;
    public static float lastZoom=0;

    public static User getCurrentUser() {
        return currentUser;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        ApplicationConfig.mContext = getApplicationContext();
    }

    public static void setCurrentUser(User currentUser) {
        ApplicationConfig.currentUser = currentUser;
    }

    public static String encodeToBase64(Bitmap image, Bitmap.CompressFormat compressFormat, int quality) {
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        image.compress(compressFormat, quality, byteArrayOS);
        return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT);
    }

    public static Context getAppContext() {
        return ApplicationConfig.mContext;
    }

    public static Bitmap decodeBase64(String input) {
        byte[] decodedBytes = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
    }

    public static LatLng ultimaTapa=null;
    public static LatLng ultimoVisto=null;

    public static Bitmap scaleDown(Bitmap realImage, float maxImageSize,
                                   boolean filter) {
        float ratio = Math.min(
                (float) maxImageSize / realImage.getWidth(),
                (float) maxImageSize / realImage.getHeight());
        int width = Math.round((float) ratio * realImage.getWidth());
        int height = Math.round((float) ratio * realImage.getHeight());

        Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, width,
                height, filter);
        return newBitmap;
    }

    /*
    * String myBase64Image = encodeToBase64(myBitmap, Bitmap.CompressFormat.JPEG, 100);
       Bitmap myBitmapAgain = decodeBase64(myBase64Image);
    * */
}
