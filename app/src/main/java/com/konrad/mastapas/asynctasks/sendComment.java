package com.konrad.mastapas.asynctasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.konrad.mastapas.ApplicationConfig;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by konra on 18/06/2017.
 */

public class sendComment extends AsyncTask<Object, String, String> {

    String user_id;
    String tapa_id;
    String descripcion;
    String precio;
    String puntuacion;
    String img1;
    String img2;
    String img3;
    Context context;

    @Override
    protected String doInBackground(Object... params) {
        try {
            user_id= (String) params [0];
            descripcion = (String) params[1];
            precio = (String) params[2];
            puntuacion = (String) params[3];
            img1 = (String) params[4];
            if(!img1.equals("")){
                img1="data:image/jpeg;base64,".concat(img1);
            }
            img2 = (String) params[5];
            if(!img2.equals("")){
                img2="data:image/jpeg;base64,".concat(img2);
            }
            img3 = (String) params[6];
            if(!img3.equals("")){
                img3="data:image/jpeg;base64,".concat(img3);
            }
            context=(Context) params[7];
            tapa_id = (String) params[8];


            Map<String, String> paramsSend = new HashMap();

            paramsSend.put("idTapa", String.valueOf(tapa_id));
            paramsSend.put("price", String.valueOf(precio));
            paramsSend.put("punctuation", String.valueOf(puntuacion));
            paramsSend.put("img1", String.valueOf(img1));
            paramsSend.put("img2", String.valueOf(img2));
            paramsSend.put("img3", String.valueOf(img3));
            paramsSend.put("idUser", String.valueOf(user_id));
            paramsSend.put("descripcion", String.valueOf(descripcion));

            JSONObject parameters = new JSONObject(paramsSend);

            String url = ApplicationConfig.URL + "newComment";

            JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.e("respùestaSegunoPlano",String.valueOf(response));
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    //501 ya existe nick o usuario ,500 error interno del servidor
                    if (error.networkResponse.statusCode == 501) {
                        Log.e("respùestaSegunoPlano",String.valueOf(error));
                    } else if (error.networkResponse.statusCode == 500) {
                        Log.e("respùestaSegunoPlano",String.valueOf(error));

                    }else{
                        Log.e("respùestaSegunoPlano",String.valueOf("err"));
                    }
                }
            }){
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    //headers.put("Content-Type", "application/json");
                    headers.put("Authorization", String.valueOf(ApplicationConfig.getCurrentUser().getApi_Token()));
                    return headers;
                }
            };

            Volley.newRequestQueue(context).add(jsonRequest);

        } catch (Exception e) {
            Log.d("GooglePlacesReadTask", e.toString());
        }
        return null;
    }

    @Override
    protected void onPostExecute(String result) {

    }

}