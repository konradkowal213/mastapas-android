package com.konrad.mastapas.dbHelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.konrad.mastapas.ApplicationConfig;
import com.konrad.mastapas.clases.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by konra on 23/05/2017.
 */

public class UserDbHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "User.db";

    public UserDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE " + User.UserEntry.TABLE_NAME + " ("
                + User.UserEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + User.UserEntry.ID + " TEXT NOT NULL,"
                + User.UserEntry.NICK + " TEXT NOT NULL,"
                + User.UserEntry.MAIL + " TEXT NOT NULL,"
                + User.UserEntry.PASS + " TEXT NOT NULL,"
                + User.UserEntry.API_TOKEN + " TEXT,"
                + "UNIQUE (" + User.UserEntry._ID + "))");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    // Adding new contact
    public void addUser(String pass) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(User.UserEntry.ID, String.valueOf(ApplicationConfig.getCurrentUser().getId_user()));
        values.put(User.UserEntry.NICK, String.valueOf(ApplicationConfig.getCurrentUser().getNick()));
        values.put(User.UserEntry.MAIL, String.valueOf(ApplicationConfig.getCurrentUser().getMail()));
        values.put(User.UserEntry.PASS, String.valueOf(pass));
        values.put(User.UserEntry.API_TOKEN, String.valueOf(ApplicationConfig.getCurrentUser().getApi_Token()));

        db.insert(User.UserEntry.TABLE_NAME, null, values);
        db.close(); // Closing database connection
    }

    // Getting All Contacts
    public List<User> getAllUsers() {
        List<User> userList = new ArrayList<User>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + User.UserEntry.TABLE_NAME;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                User user = new User();
                user.setId_user(cursor.getString(1));
                user.setMail(cursor.getString(3));
                user.setPass(cursor.getString(4));
                user.setApi_Token(cursor.getString(5));
                user.setNick(cursor.getString(2));
                // Adding contact to list
                userList.add(user);
            } while (cursor.moveToNext());
        }

        // return contact list
        return userList;
    }

    // Deleting single contact
    public void deleteUser(String user) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(User.UserEntry.TABLE_NAME, User.UserEntry.ID + " = ?",
                new String[]{String.valueOf(user)});
        db.close();
    }

    public void deleteAllUsers() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(User.UserEntry.TABLE_NAME, null, null);
        db.close();
    }

    public int updateUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(User.UserEntry.NICK, String.valueOf(ApplicationConfig.getCurrentUser().getNick()));
        values.put(User.UserEntry.MAIL, String.valueOf(ApplicationConfig.getCurrentUser().getMail()));
        values.put(User.UserEntry.API_TOKEN, String.valueOf(ApplicationConfig.getCurrentUser().getApi_Token()));
        values.put(User.UserEntry.PASS, String.valueOf(ApplicationConfig.getCurrentUser().getPass()));

        // updating row
        return db.update(User.UserEntry.TABLE_NAME, values, User.UserEntry.ID + " = ?",
                new String[]{String.valueOf(user.getId_user())});
    }

    public int updateTokenUser(User user,String token) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(User.UserEntry.API_TOKEN, String.valueOf(token));

        // updating row
        return db.update(User.UserEntry.TABLE_NAME, values, User.UserEntry.ID + " = ?",
                new String[] { String.valueOf(user.getId_user()) });
    }
}
