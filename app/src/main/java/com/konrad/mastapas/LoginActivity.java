package com.konrad.mastapas;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.konrad.mastapas.clases.User;
import com.konrad.mastapas.dbHelper.UserDbHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    private EditText email, pass;
    private Button login;
    private TextView newAccount, newPassword;
    private User user=new User();
    private CheckBox guardar;
    UserDbHelper db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        newAccount=(TextView) findViewById(R.id.link_signup);
        newAccount.setTextColor(Color.WHITE);
        newAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent newAccount = new Intent(getApplicationContext(), newAccount.class);
                startActivity(newAccount);
                finish();
            }
        });
        newPassword=(TextView) findViewById(R.id.pass);
        newPassword.setTextColor(Color.WHITE);
        newPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent newPass = new Intent(getApplicationContext(), newPass.class);
                startActivity(newPass);
                finish();
            }
        });
        login=(Button) findViewById(R.id.btn_login);
        guardar=(CheckBox) findViewById(R.id.remember);
        guardar.setTextColor(Color.WHITE);
        email=(EditText) findViewById(R.id.input_email);
        email.setHighlightColor(Color.WHITE);
        email.setTextColor(Color.WHITE);
        email.setHintTextColor(Color.WHITE);
        pass=(EditText) findViewById(R.id.input_password);
        pass.setHighlightColor(Color.WHITE);
        pass.setTextColor(Color.WHITE);
        final ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this, R.style.Theme_AppCompat_DayNight_Dialog);
        if ((ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)){
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 123);
        }

        db=new UserDbHelper(this);
        List<User> userList = new ArrayList<User>();
        userList=db.getAllUsers();
        progressDialog.setMessage("Iniciando sesión...");
        progressDialog.show();
        if(userList.size()==0){
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Iniciando sesión...");
            progressDialog.dismiss();
        }else{
            email.setText(userList.get(userList.size()-1).getMail());
            ApplicationConfig.setCurrentUser(userList.get(0));
            Log.e("tamaño users", String.valueOf(userList.size()));
            loginIfExist(userList.get(userList.size()-1),progressDialog);
        }
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog.setIndeterminate(true);
                progressDialog.setCancelable(false);
                progressDialog.setMessage("Iniciando sesión...");
                progressDialog.show();
                login(progressDialog);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void loginIfExist(User userM , final ProgressDialog progressDialog){
        String url = ApplicationConfig.URL+"login";
        db.deleteUser(String.valueOf(user.getId_user()));

        Map<String, String> params = new HashMap();
        params.put("mail", String.valueOf(userM.getMail()));
        params.put("pass", String.valueOf(userM.getPass()));

        JSONObject parameters = new JSONObject(params);

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                try {
                    Log.e("JSON", String.valueOf(response.getString("ok")));
                    user.setApi_Token(String.valueOf(response.getString("ok")));
                    getUserData(user.getApi_Token(),progressDialog);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.e("Error",String.valueOf(error.networkResponse));
                if(error.networkResponse.statusCode==401){
                    db.deleteAllUsers();
                    new AlertDialog.Builder(LoginActivity.this)
                            .setTitle("Error")
                            .setMessage("Usuario o contraseña incorectos")
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .show();
                }else{
                    new AlertDialog.Builder(LoginActivity.this)
                            .setTitle("Ups!")
                            .setMessage("Error de conexion con el servidor, intentelo mas tarde")
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .show();
                }

            }
        });
        int socketTimeout = 15000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsonRequest.setRetryPolicy(policy);
        Volley.newRequestQueue(this).add(jsonRequest);
    }

    public void login(final ProgressDialog progressDialog){
        String url = ApplicationConfig.URL+"login";

        Map<String, String> params = new HashMap();
        params.put("mail", String.valueOf(email.getText()));
        params.put("pass", String.valueOf(pass.getText()));

        JSONObject parameters = new JSONObject(params);

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                try {
                    Log.e("JSON", String.valueOf(response.getString("ok")));
                    user.setApi_Token(String.valueOf(response.getString("ok")));
                    user.setMail(String.valueOf(email.getText()));
                    user.setPass(String.valueOf(pass.getText()));
                    ApplicationConfig.getCurrentUser().setApi_Token(user.getApi_Token());
                    ApplicationConfig.getCurrentUser().setMail(user.getMail());
                    ApplicationConfig.getCurrentUser().setPass(user.getPass());
                    getUserData(user.getApi_Token(),progressDialog);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                if(error.getCause()==null){
                    if(error.networkResponse.statusCode==401){
                        new AlertDialog.Builder(LoginActivity.this)
                                .setTitle("Error")
                                .setMessage("Usuario o contraseña incorectos")
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                })
                                .show();
                    }else{
                        new AlertDialog.Builder(LoginActivity.this)
                                .setTitle("Ups!")
                                .setMessage("Error de conexion con el servidor, intentelo mas tarde")
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                })
                                .show();
                    }
                }else{
                    new AlertDialog.Builder(LoginActivity.this)
                            .setTitle("Ups!")
                            .setMessage("Error de conexion con el servidor, intentelo mas tarde")
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .show();
                }

            }
        });
        int socketTimeout = 15000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsonRequest.setRetryPolicy(policy);
        Volley.newRequestQueue(this).add(jsonRequest);
    }

    public void getUserData(final String token, final ProgressDialog progressDialog){
        String url = ApplicationConfig.URL+"userData";

        JsonArrayRequest jsonRequest = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.e("JSON", String.valueOf(response));
                try {
                    user.setId_user(String.valueOf(response.getJSONObject(0).getString("ID_user")));
                    user.setNick(String.valueOf(response.getJSONObject(0).getString("Nick")));
                    user.setToken_expiration(response.getJSONObject(0).getString("TOKEN_expiracion"));
                    ApplicationConfig.getCurrentUser().setId_user(user.getId_user());
                    ApplicationConfig.getCurrentUser().setNick(user.getNick());
                    ApplicationConfig.getCurrentUser().setToken_expiration(user.getToken_expiration());
                    ApplicationConfig.getCurrentUser().setApi_Token(token);
                    if(ApplicationConfig.getCurrentUser().getApi_Token().equals(token)){
                        Log.e("i","a");
                    }else{
                        db.updateTokenUser(ApplicationConfig.getCurrentUser(),token);
                    }
                    progressDialog.dismiss();
                    if(guardar.isChecked()){
                       db.addUser(String.valueOf(pass.getText()));
                    }else{
                        db.updateUser(ApplicationConfig.getCurrentUser());
                    }

//                    Intent map = new Intent(getApplicationContext(), splash.class);
                    Intent map = new Intent(getApplicationContext(), ActivityPrincipal.class);

                    startActivity(map);
                    finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("JSON:", String.valueOf(error));
                progressDialog.dismiss();
                if(error.getCause()!=null){
                    if(error.networkResponse.statusCode==401){
                        new AlertDialog.Builder(LoginActivity.this)
                                .setTitle("Error")
                                .setMessage("Usuario o contraseña incorectos")
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                })
                                .show();
                    }else{
                        new AlertDialog.Builder(LoginActivity.this)
                                .setTitle("Ups!")
                                .setMessage("Error de conexion con el servidor, intentelo mas tarde")
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                })
                                .show();
                    }
                }else{
                    new AlertDialog.Builder(LoginActivity.this)
                            .setTitle("Ups!")
                            .setMessage("Error de conexion con el servidor, intentelo mas tarde")
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .show();
                }
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/json");
                headers.put("Authorization", String.valueOf(token));
                return headers;
            }
        };
        int socketTimeout = 15000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsonRequest.setRetryPolicy(policy);
        Volley.newRequestQueue(this).add(jsonRequest);
    }

    public AlertDialog createCloseDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);

        LayoutInflater inflater = this.getLayoutInflater();

        View v = inflater.inflate(R.layout.fragment_closeapplication, null);

        builder.setView(v);

        final TextView cancelar = (TextView) v.findViewById(R.id.cancel);
        final TextView aceptar = (TextView) v.findViewById(R.id.cerrarApp);

        final AlertDialog d = builder.show();

        aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                android.os.Process.killProcess(android.os.Process.myPid());
            }
        });

        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                d.dismiss();
            }
        });
        builder.setCancelable(false);

        return builder.create();
    }



}
