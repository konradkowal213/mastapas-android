package com.konrad.mastapas.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.konrad.mastapas.ApplicationConfig;
import com.konrad.mastapas.R;
import com.konrad.mastapas.clases.Images;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class DescriptionFragment extends Fragment {
    Button next, previous;
    String descripcion;
    String precio;
    String fecha;
    String puntuacion;
    TextView desc, price, date;
    RatingBar rat;
    ImageView imageView;
    String idDescripcion;
    ArrayList<Bitmap> arrayImagenes=new ArrayList<>();
    int tamannoArray;
    int c=0;
    ProgressBar descargando;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_description, container, false);
        if (getArguments() != null) {
            descripcion = getArguments().getString("value_DESC");
            precio = getArguments().getString("value_PRICE");
            fecha = getArguments().getString("value_DATE");
            puntuacion = getArguments().getString("value_RAT");
            idDescripcion = getArguments().getString("value_ID");
        }
        descargando=(ProgressBar) v.findViewById(R.id.descargandoImages);
        descargando.setVisibility(View.VISIBLE);

        next=(Button) v.findViewById(R.id.button_next);
        next.setEnabled(false);
        previous=(Button) v.findViewById(R.id.button_previous);
        previous.setEnabled(false);
        desc=(TextView) v.findViewById(R.id.IDescripcion);
        price=(TextView)v.findViewById(R.id.priceDesc) ;
        date=(TextView) v.findViewById(R.id.dateDesc) ;
        rat=(RatingBar) v.findViewById(R.id.puntuacionDes);
        desc.setText(descripcion);
        price.setText(precio+" €");
        String fechaSeparada[]=fecha.split(" ");
        date.setText("Publicado el "+ fechaSeparada[0] +" a las "+fechaSeparada[1]);
        rat.setRating(Float.parseFloat(String.valueOf(puntuacion)));
        imageView = (ImageView) v.findViewById(R.id.imageSwitcher);
        cargarImagenes(idDescripcion);

        return v;
    }

    private void cargarImagenes(String id) {
        String url = ApplicationConfig.URL + "getImages/"+id;

        JsonArrayRequest jsonRequest = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                descargando.setVisibility(View.GONE);
                if(response.length()==0){
                    imageView.setImageResource(R.drawable.noimage);
                }else{
                    for (int f = 0; f < response.length(); f++) {
                        try {
                            Images img=new Images(response.getJSONObject(f));
                            String base64Image[]=img.getBase64_img().split(",");
                            arrayImagenes.add(ApplicationConfig.decodeBase64(base64Image[1]));
                            if(f==0){
                                imageView.setImageBitmap(arrayImagenes.get(0));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    tamannoArray=arrayImagenes.size();
                    if(tamannoArray==1){
                        next.setEnabled(false);
                    }else{
                        next.setEnabled(true);
                    }
                    next.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            changeImageView("n");
                        }
                    });
                    previous.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            changeImageView("p");
                        }
                    });
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                descargando.setVisibility(View.GONE);
                Log.e("JSON:", String.valueOf(error));
                new AlertDialog.Builder(getActivity())
                        .setTitle("Error")
                        .setMessage("Error al cargar las fotos, intentelo mas tarde")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/json");
                headers.put("Authorization", String.valueOf(ApplicationConfig.getCurrentUser().getApi_Token()));
                return headers;
            }
        };
        Volley.newRequestQueue(getActivity()).add(jsonRequest);
    }

    private void changeImageView(String value){
        if(value.equals("n")){
            if(c<arrayImagenes.size()){
                c++;
                imageView.setImageBitmap(arrayImagenes.get(c));
            }
            previous.setEnabled(true);
        }else{//p
            if(c>0){
                c--;
                imageView.setImageBitmap(arrayImagenes.get(c));
            }
        }
        if(c==0){
            previous.setEnabled(false);
        }else{
            previous.setEnabled(true);
        }
        if(c==arrayImagenes.size()-1){
            next.setEnabled(false);
        }else{
            next.setEnabled(true);
        }
    }











}
