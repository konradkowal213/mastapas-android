package com.konrad.mastapas.fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationListener;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.konrad.mastapas.ApplicationConfig;
import com.konrad.mastapas.BuildConfig;
import com.konrad.mastapas.R;
import com.konrad.mastapas.asynctasks.sendComment;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static android.app.Activity.RESULT_OK;


public class NewCommentFragment extends Fragment implements OnMapReadyCallback,  DialogInterface.OnClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener, AdapterView.OnItemClickListener,PlaceSelectionListener {

    RatingBar ratingBarDesc;
    TextView textView,textView2,textView3, descripcionTextView,precioDesc;
    Button sendButton;
    private static final int CAMERA_REQUEST_CODE = 0;
    private static final int GALLERY_REQUEST_CODE = 1;
    String mCurrentPhotoPath;
    private static final int REQUEST_CODE = 11;
    private static int selectedTextView=0;
    String search = "bar|cafe|meal_delivery|meal_takeaway|campground|establishment|casino|church|point_of_interest|night_club|restaurant|subway_station|train_station|transit_station|university|food";

    ArrayList<String> errores=new ArrayList<>();

    String idTapa;
    String user_id;
    String descripcion;
    String precio;
    String puntuacion;
    String img1="";
    String img2="";
    String img3="";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstBundle) {
        View v = inflater.inflate(R.layout.fragment_new_comment, container, false);
        if(v==null){

        }
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            idTapa = bundle.getString("value_ID");
        }
        sendButton=(Button) v.findViewById(R.id.enviar);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validar()){
                    if(ratingBarDesc.getRating()==0){
                        final AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
                        dialog.setCancelable(true);
                        dialog.setTitle("La puntuación...");
                        dialog.setMessage(String.valueOf("No has puesto puntuacion... es por que se te ha olvidado o por que es muy malo?"));
                        dialog.setPositiveButton("Ups, se me olvidó", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                               dialogInterface.dismiss();
                            }
                        });
                        dialog.setNegativeButton("Es muy malo", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                enviar();
                            }
                        });
                        dialog.show();
                    }else{
                        enviar();
                    }

                }
            }
        });
        ratingBarDesc=(RatingBar) v.findViewById(R.id.ratingBarDesc);
        precioDesc=(TextView) v.findViewById(R.id.priceDescription);
        descripcionTextView=(TextView) v.findViewById(R.id.input_descripcion);

        textView=(TextView) v.findViewById(R.id.photo);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedTextView=0;
                selectImage();
            }
        });
        textView2=(TextView) v.findViewById(R.id.photo2);
        textView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedTextView=1;
                selectImage();
            }
        });
        textView3=(TextView) v.findViewById(R.id.photo3);
        textView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedTextView=2;
                selectImage();
            }
        });

        return v;
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try{
            if(requestCode == CAMERA_REQUEST_CODE) {
                Log.e("ResultCode: ",String.valueOf(resultCode));
                if (resultCode != RESULT_OK || resultCode == RESULT_OK) {
                    if(selectedTextView==0){
                        textView.setText("Procesando imagen...");
                    }else if(selectedTextView==1){
                        textView2.setText("Procesando imagen...");
                    }else if(selectedTextView==2){
                        textView3.setText("Procesando imagen...");
                    }
                    new Thread(new Runnable() {
                        public void run() {
                            Bitmap bitmap = null;
                            try {
                                bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), Uri.parse(mCurrentPhotoPath));
                            } catch (IOException e) {

                            }
                            if(bitmap!=null){
                                if((bitmap.getHeight()>4096)|| bitmap.getWidth()>4096){
                                    bitmap=ApplicationConfig.scaleDown(bitmap,(bitmap.getHeight()/2),true);
                                }
                                final String s=ApplicationConfig.encodeToBase64(bitmap, Bitmap.CompressFormat.JPEG,70);
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if(selectedTextView==0){
                                            textView.setText("Primera seleccionada");
                                            com.vansuita.library.Icon.left(textView).icon(R.drawable.checked).color(R.color.colorGreen).put();
                                            textView2.setVisibility(View.VISIBLE);
                                            img1=s;
                                        }else if(selectedTextView==1){
                                            textView2.setText("Segunda seleccionada");
                                            com.vansuita.library.Icon.left(textView2).icon(R.drawable.checked).color(R.color.colorGreen).put();
                                            textView3.setVisibility(View.VISIBLE);
                                            img2=s;
                                        }else if(selectedTextView==2){
                                            textView3.setText("Tercera seleccionada");
                                            com.vansuita.library.Icon.left(textView3).icon(R.drawable.checked).color(R.color.colorGreen).put();
                                            img3=s;
                                        }

                                    }
                                });
                            }else{
                                if(selectedTextView==0){
                                    textView.setText("Selecciona la primera imagen pulsando aquí");
                                }else if(selectedTextView==1){
                                    textView2.setText("Selecciona la segunda imagen pulsando aquí");
                                }else if(selectedTextView==2){
                                    textView3.setText("Selecciona la tercera imagen pulsando aquí");
                                }
                            }


                        }
                    }).start();

                }
            } else {
                if (resultCode == RESULT_OK) {
                    if(selectedTextView==0){
                        textView.setText("Procesando imagen...");
                    }else if(selectedTextView==1){
                        textView2.setText("Procesando imagen...");
                    }else if(selectedTextView==2){
                        textView3.setText("Procesando imagen...");
                    }
                    new Thread(new Runnable() {
                        public void run() {
                            Bitmap bitmap = null;
                            try {
                                Uri selectedImage = data.getData();
                                bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), Uri.parse(String.valueOf(selectedImage)));

                            } catch (IOException e) {
                                e.printStackTrace();
                            }if(bitmap!=null){
                                if((bitmap.getHeight()>4096)|| bitmap.getWidth()>4096){
                                    bitmap=ApplicationConfig.scaleDown(bitmap,(bitmap.getHeight()/2),true);
                                }
                                final String s=ApplicationConfig.encodeToBase64(bitmap, Bitmap.CompressFormat.JPEG,30);
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if(selectedTextView==0){
                                            textView.setText("Primera seleccionada");
                                            com.vansuita.library.Icon.left(textView).icon(R.drawable.checked).color(R.color.colorGreen).put();
                                            textView2.setVisibility(View.VISIBLE);
                                            img1=s;
                                        }else if(selectedTextView==1){
                                            textView2.setText("Segunda seleccionada");
                                            com.vansuita.library.Icon.left(textView2).icon(R.drawable.checked).color(R.color.colorGreen).put();
                                            textView3.setVisibility(View.VISIBLE);
                                            img2=s;

                                        }else if(selectedTextView==2){
                                            textView3.setText("Tercera seleccionada");
                                            img3=s;

                                            com.vansuita.library.Icon.left(textView3).icon(R.drawable.checked).color(R.color.colorGreen).put();
                                        }

                                    }
                                });
                            }else{

                            }


                        }
                    }).start();

                }
            }
        }catch (Exception e){}
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM), "Camera");
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }




    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    protected void selectImage() {
        final CharSequence[] options = { "Cámara", "Galería" };
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Completar acción utilizando");
        builder.setItems(options, this);
        builder.show();
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int which) {

        if(which == 0) {
            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                this.requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, REQUEST_CODE);
            }else {
                intentTakePhoto();
            }
        }else {
            intentPickPhoto();
        }

    }

    private void intentTakePhoto() {

        Intent mIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        Uri fileUri = null;
        try {
            fileUri = FileProvider.getUriForFile(getActivity(), BuildConfig.APPLICATION_ID + ".provider", createImageFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
        mIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri); // set the image file name

            List<ResolveInfo> resInfoList = getActivity().getPackageManager().queryIntentActivities(mIntent, PackageManager.MATCH_DEFAULT_ONLY);
            for (ResolveInfo resolveInfo : resInfoList) {
                String packageName = resolveInfo.activityInfo.packageName;
                getActivity().grantUriPermission(packageName, fileUri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
            }
            startActivityForResult(mIntent, CAMERA_REQUEST_CODE);
    }

    private void intentPickPhoto() {
        Intent mIntent;

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            mIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            mIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            mIntent.addFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
        }else{
            mIntent = new Intent(Intent.ACTION_GET_CONTENT);
        }
        mIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        mIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        mIntent.setType("image/*");

        Intent pickIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickIntent.setType("image/*");

        Intent chooserIntent = Intent.createChooser(mIntent, "Select Image");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] {pickIntent});
        startActivityForResult(chooserIntent, GALLERY_REQUEST_CODE);

    }

    boolean validar(){
        errores.clear();
        if(descripcionTextView.getText().toString().equals("")){
            errores.add("No has escrito ninguna descripción");
        }
        if(precioDesc.getText().toString().equals("")){
            errores.add("No has puesto el precio que has pagado por todo");
        }
        if(img1.equals("")){
            errores.add("Es obligatorio seleccionar al menos una imagen");
        }
        if(!errores.isEmpty()){
            String text="";
            for( int f=0;f<errores.size();f++){
                if(f==0){
                    text="-"+errores.get(f);
                }else{
                    text=text+"\n\n"+"-"+errores.get(f);
                }
            }
            final AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
            dialog.setCancelable(true);
            dialog.setTitle("Ups! Tienes que rellenar ciertos campos...");
            dialog.setMessage(String.valueOf(text));
            dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            dialog.show();
            return false;
        }else{
            return true;
        }

    }

    private void enviar(){
        user_id=ApplicationConfig.getCurrentUser().getId_user();
        descripcion=String.valueOf(descripcionTextView.getText());
        precio=String.valueOf(precioDesc.getText());
        puntuacion=String.valueOf(ratingBarDesc.getRating());
        Object[] DataTransfer = new Object[12];
        DataTransfer[0] = user_id;
        DataTransfer[1] = descripcion;
        DataTransfer[2] = precio;
        DataTransfer[3] = puntuacion;
        DataTransfer[4] = img1;
        DataTransfer[5] = img2;
        DataTransfer[6] = img3;
        DataTransfer[7] = getContext();
        DataTransfer[8] = idTapa;
        final AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setCancelable(true);
        dialog.setTitle("GRACIAS!");
        dialog.setMessage(String.valueOf("Gracias por colaborar! En cuanto tu publicación sea verificada, usted y toda la comunidad podrá visualizarla en el mapa. \n\n" +
                "Mientras tanto, puede consultar el estado de su publicación en el apartado de 'Mis publicaciones'."));
        dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                getActivity().onBackPressed();
            }
        });

        dialog.show();
        sendComment send= new sendComment();
        send.execute(DataTransfer);
    }


    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onPlaceSelected(Place place) {

    }

    @Override
    public void onError(Status status) {

    }
}
