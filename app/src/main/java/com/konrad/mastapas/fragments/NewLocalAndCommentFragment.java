package com.konrad.mastapas.fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.konrad.mastapas.ApplicationConfig;
import com.konrad.mastapas.BuildConfig;
import com.konrad.mastapas.R;
import com.konrad.mastapas.asynctasks.sendCommentAndDescription;
import com.konrad.mastapas.clases.GetNearbyTapasData;
import com.konrad.mastapas.clases.Tapa;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.LOCATION_SERVICE;


public class NewLocalAndCommentFragment extends Fragment implements OnMapReadyCallback,  DialogInterface.OnClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener, AdapterView.OnItemClickListener,PlaceSelectionListener {
    private static Tapa tapa;
    TextView textoOrdenar;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    double latitude;
    double longitude;
    ListView listViewPlaces;
    LocationManager locManager;
    RatingBar ratingBarDesc;
    private int PROXIMITY_RADIUS = 200;
    TextView textView,textView2,textView3, descripcionTextView,precioDesc;
    Button sendButton;
    private static final int CAMERA_REQUEST_CODE = 0;
    private static final int GALLERY_REQUEST_CODE = 1;
    public static PlaceAutocompleteFragment autocompleteFragment;
    String mCurrentPhotoPath;
    private static final int REQUEST_CODE = 11;
    private static int selectedTextView=0;
    String search = "bar|cafe|meal_delivery|meal_takeaway|campground|establishment|casino|church|point_of_interest|night_club|restaurant|subway_station|train_station|transit_station|university|food";

    ArrayList<String> errores=new ArrayList<>();

    String nombre;
    String calle;
    String latitud;
    String longitud;
    String user_id;
    String tapa_id;
    String descripcion;
    String precio;
    String puntuacion;
    String img1="";
    String img2="";
    String img3="";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstBundle) {
        View v = inflater.inflate(R.layout.fragment_new_local_and_comment, container, false);
        if(v==null){

        }
        sendButton=(Button) v.findViewById(R.id.enviar);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validar()){
                    if(ratingBarDesc.getRating()==0){
                        final AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
                        dialog.setCancelable(true);
                        dialog.setTitle("La puntuación...");
                        dialog.setMessage(String.valueOf("No has puesto puntuacion... es por que se te ha olvidado o por que es muy malo?"));
                        dialog.setPositiveButton("Ups, se me olvidó", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                               dialogInterface.dismiss();
                            }
                        });
                        dialog.setNegativeButton("Es muy malo", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                enviar();
                            }
                        });
                        dialog.show();
                    }else{
                        enviar();
                    }

                }
            }
        });
        ratingBarDesc=(RatingBar) v.findViewById(R.id.ratingBarDesc);
        locManager= (LocationManager)getContext().getSystemService(LOCATION_SERVICE);
        precioDesc=(TextView) v.findViewById(R.id.priceDescription);
        descripcionTextView=(TextView) v.findViewById(R.id.input_descripcion);
        listViewPlaces=(ListView) v.findViewById(R.id.listaConUbicaciones);
        textoOrdenar=(TextView) v.findViewById(R.id.textoSitiosOrdenadosPor);
        listViewPlaces.setOnItemClickListener(this);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }
        textView=(TextView) v.findViewById(R.id.photo);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedTextView=0;
                selectImage();
            }
        });
        textView2=(TextView) v.findViewById(R.id.photo2);
        textView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedTextView=1;
                selectImage();
            }
        });
        textView3=(TextView) v.findViewById(R.id.photo3);
        textView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedTextView=2;
                selectImage();
            }
        });
        locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,1000L,500.0f, this);
        locManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,1000L,500.0f, this);

        autocompleteFragment  = (PlaceAutocompleteFragment)getActivity().getFragmentManager().findFragmentById(R.id.place_fragment);
        autocompleteFragment.setOnPlaceSelectedListener(this);
        autocompleteFragment.getView().findViewById(R.id.place_autocomplete_clear_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                autocompleteFragment.setText("");
                if(listViewPlaces.getCount()!=0){
                    listViewPlaces.setVisibility(View.VISIBLE);
                }
                textoOrdenar.setVisibility(View.VISIBLE);
            }
        });
        autocompleteFragment.setHint("Nombre del establecimiento");

        return v;
    }


    @Override
    public void onLocationChanged(Location location) {
        latitude = location.getLatitude();
        longitude = location.getLongitude();
        String url = getUrl(latitude, longitude, search);
        Object[] DataTransfer = new Object[6];
        DataTransfer[0] = url;
        DataTransfer[1] = location;
        DataTransfer[2] = listViewPlaces;
        DataTransfer[3] = getContext();
        DataTransfer[4] = textoOrdenar;
        DataTransfer[5] = NewLocalAndCommentFragment.this;
        GetNearbyTapasData getNearbyTapasData = new GetNearbyTapasData();
        getNearbyTapasData.execute(DataTransfer);
    }




    private String getUrl(double latitude, double longitude, String nearbyPlace) {
        StringBuilder googlePlacesUrl = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
        googlePlacesUrl.append("location=" + latitude + "," + longitude);
        googlePlacesUrl.append("&radius=" + PROXIMITY_RADIUS);
        googlePlacesUrl.append("&rankBy=distance");
        //googlePlacesUrl.append("&type=" + nearbyPlace);
        googlePlacesUrl.append("&key=" + "AIzaSyCbuDuF-uc5kizHwerUBBU4wfPe87gmrpU");
        return (googlePlacesUrl.toString());
    }

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION},MY_PERMISSIONS_REQUEST_LOCATION);
            } else {
                ActivityCompat.requestPermissions(getActivity(),  new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Log.e("e",String.valueOf(i)); //En segundo plano, coger latitud, longitud y calle
        tapa=new Tapa(ApplicationConfig.tapasCercania.get(i).getNombre(),ApplicationConfig.tapasCercania.get(i).getCalle(),ApplicationConfig.tapasCercania.get(i).getLatitud(),ApplicationConfig.tapasCercania.get(i).getLongitud());
        autocompleteFragment.setText(String.valueOf(tapa.getNombre()));
    }

    @Override
    public void onPlaceSelected(Place place) {
        Log.i("LOG PLACES", "Place Selected: " + place.getName());
        tapa=new Tapa(String.valueOf(place.getName()),String.valueOf(place.getAddress()),String.valueOf(place.getLatLng().latitude),String.valueOf(place.getLatLng()));
        if (!TextUtils.isEmpty(place.getAttributions())){
        }
    }

    @Override
    public void onError(Status status) {
        Log.e("error", "onError: Status = " + status.toString());
        Toast.makeText(getContext(), "Place selection failed: " + status.getStatusMessage(),
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try{
            if(requestCode == CAMERA_REQUEST_CODE) {
                Log.e("ResultCode: ",String.valueOf(resultCode));
                if (resultCode != RESULT_OK || resultCode == RESULT_OK) {
                    if(selectedTextView==0){
                        textView.setText("Procesando imagen...");
                    }else if(selectedTextView==1){
                        textView2.setText("Procesando imagen...");
                    }else if(selectedTextView==2){
                        textView3.setText("Procesando imagen...");
                    }
                    new Thread(new Runnable() {
                        public void run() {
                            Bitmap bitmap = null;
                            try {
                                bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), Uri.parse(mCurrentPhotoPath));
                            } catch (IOException e) {

                            }
                            if(bitmap!=null){
                                if((bitmap.getHeight()>4096)|| bitmap.getWidth()>4096){
                                    bitmap=ApplicationConfig.scaleDown(bitmap,(bitmap.getHeight()/2),true);
                                }
                                final String s=ApplicationConfig.encodeToBase64(bitmap, Bitmap.CompressFormat.JPEG,70);
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if(selectedTextView==0){
                                            textView.setText("Primera seleccionada");
                                            com.vansuita.library.Icon.left(textView).icon(R.drawable.checked).color(R.color.colorGreen).put();
                                            textView2.setVisibility(View.VISIBLE);
                                            img1=s;
                                        }else if(selectedTextView==1){
                                            textView2.setText("Segunda seleccionada");
                                            com.vansuita.library.Icon.left(textView2).icon(R.drawable.checked).color(R.color.colorGreen).put();
                                            textView3.setVisibility(View.VISIBLE);
                                            img2=s;
                                        }else if(selectedTextView==2){
                                            textView3.setText("Tercera seleccionada");
                                            com.vansuita.library.Icon.left(textView3).icon(R.drawable.checked).color(R.color.colorGreen).put();
                                            img3=s;
                                        }

                                    }
                                });
                            }else{
                                if(selectedTextView==0){
                                    textView.setText("Selecciona la primera imagen pulsando aquí");
                                }else if(selectedTextView==1){
                                    textView2.setText("Selecciona la segunda imagen pulsando aquí");
                                }else if(selectedTextView==2){
                                    textView3.setText("Selecciona la tercera imagen pulsando aquí");
                                }
                            }


                        }
                    }).start();

                }
            } else {
                if (resultCode == RESULT_OK) {
                    if(selectedTextView==0){
                        textView.setText("Procesando imagen...");
                    }else if(selectedTextView==1){
                        textView2.setText("Procesando imagen...");
                    }else if(selectedTextView==2){
                        textView3.setText("Procesando imagen...");
                    }
                    new Thread(new Runnable() {
                        public void run() {
                            Bitmap bitmap = null;
                            try {
                                Uri selectedImage = data.getData();
                                bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), Uri.parse(String.valueOf(selectedImage)));

                            } catch (IOException e) {
                                e.printStackTrace();
                            }if(bitmap!=null){
                                if((bitmap.getHeight()>4096)|| bitmap.getWidth()>4096){
                                    bitmap=ApplicationConfig.scaleDown(bitmap,(bitmap.getHeight()/2),true);
                                }
                                final String s=ApplicationConfig.encodeToBase64(bitmap, Bitmap.CompressFormat.JPEG,30);
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if(selectedTextView==0){
                                            textView.setText("Primera seleccionada");
                                            com.vansuita.library.Icon.left(textView).icon(R.drawable.checked).color(R.color.colorGreen).put();
                                            textView2.setVisibility(View.VISIBLE);
                                            img1=s;
                                        }else if(selectedTextView==1){
                                            textView2.setText("Segunda seleccionada");
                                            com.vansuita.library.Icon.left(textView2).icon(R.drawable.checked).color(R.color.colorGreen).put();
                                            textView3.setVisibility(View.VISIBLE);
                                            img2=s;

                                        }else if(selectedTextView==2){
                                            textView3.setText("Tercera seleccionada");
                                            img3=s;

                                            com.vansuita.library.Icon.left(textView3).icon(R.drawable.checked).color(R.color.colorGreen).put();
                                        }

                                    }
                                });
                            }else{

                            }


                        }
                    }).start();

                }
            }
        }catch (Exception e){}
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM), "Camera");
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }


    private Bitmap getBitmap(String path) {

        Uri uri = Uri.fromFile(new File(path));
        InputStream in = null;
        try {
            final int IMAGE_MAX_SIZE = 1200000; // 1.2MP
            in = getActivity().getContentResolver().openInputStream(uri);

            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(in, null, o);
            in.close();


            int scale = 1;
            while ((o.outWidth * o.outHeight) * (1 / Math.pow(scale, 2)) >
                    IMAGE_MAX_SIZE) {
                scale++;
            }
            Log.d("", "scale = " + scale + ", orig-width: " + o.outWidth + ", orig-height: " + o.outHeight);

            Bitmap b = null;
            in = getActivity().getContentResolver().openInputStream(uri);
            if (scale > 1) {
                scale--;
                // scale to max possible inSampleSize that still yields an image
                // larger than target
                o = new BitmapFactory.Options();
                o.inSampleSize = scale;
                b = BitmapFactory.decodeStream(in, null, o);

                // resize to desired dimensions
                int height = b.getHeight();
                int width = b.getWidth();
                Log.d("", "1th scale operation dimenions - width: " + width + ", height: " + height);

                double y = Math.sqrt(IMAGE_MAX_SIZE
                        / (((double) width) / height));
                double x = (y / height) * width;

                Bitmap scaledBitmap = Bitmap.createScaledBitmap(b, (int) x,
                        (int) y, true);
                b.recycle();
                b = scaledBitmap;

                System.gc();
            } else {
                b = BitmapFactory.decodeStream(in);
            }
            in.close();

            Log.d("", "bitmap size - width: " + b.getWidth() + ", height: " +
                    b.getHeight());
            return b;
        } catch (IOException e) {
            Log.e("", e.getMessage(), e);
            return null;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        android.app.FragmentTransaction ft = getActivity().getFragmentManager().beginTransaction();
        ft.remove(autocompleteFragment);
        ft.commit();
    }

    protected void selectImage() {
        final CharSequence[] options = { "Cámara", "Galería" };
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Completar acción utilizando");
        builder.setItems(options, this);
        builder.show();
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int which) {

        if(which == 0) {
            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                this.requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, REQUEST_CODE);
            }else {
                intentTakePhoto();
            }
        }else {
            intentPickPhoto();
        }

    }

    private void intentTakePhoto() {

        Intent mIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        Uri fileUri = null;
        try {
            fileUri = FileProvider.getUriForFile(getActivity(), BuildConfig.APPLICATION_ID + ".provider", createImageFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
        mIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri); // set the image file name

            List<ResolveInfo> resInfoList = getActivity().getPackageManager().queryIntentActivities(mIntent, PackageManager.MATCH_DEFAULT_ONLY);
            for (ResolveInfo resolveInfo : resInfoList) {
                String packageName = resolveInfo.activityInfo.packageName;
                getActivity().grantUriPermission(packageName, fileUri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
            }
            startActivityForResult(mIntent, CAMERA_REQUEST_CODE);
    }

    private void intentPickPhoto() {
        Intent mIntent;

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            mIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            mIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            mIntent.addFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
        }else{
            mIntent = new Intent(Intent.ACTION_GET_CONTENT);
        }
        mIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        mIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        mIntent.setType("image/*");

        Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickIntent.setType("image/*");

        Intent chooserIntent = Intent.createChooser(mIntent, "Select Image");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] {pickIntent});
        startActivityForResult(chooserIntent, GALLERY_REQUEST_CODE);

    }

    boolean validar(){
        errores.clear();
        if(tapa==null){
            errores.add("No has seleccionado ningun lugar de tapas");
        }
        if(descripcionTextView.getText().toString().equals("")){
            errores.add("No has escrito ninguna descripción");
        }
        if(precioDesc.getText().toString().equals("")){
            errores.add("No has puesto el precio que has pagado por todo");
        }
        if(img1.equals("")){
            errores.add("Es obligatorio seleccionar al menos una imagen");
        }
        if(!errores.isEmpty()){
            String text="";
            for( int f=0;f<errores.size();f++){
                if(f==0){
                    text="-"+errores.get(f);
                }else{
                    text=text+"\n\n"+"-"+errores.get(f);
                }
            }
            final AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
            dialog.setCancelable(true);
            dialog.setTitle("Ups! Tienes que rellenar ciertos campos...");
            dialog.setMessage(String.valueOf(text));
            dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            dialog.show();
            return false;
        }else{
            return true;
        }

    }

    private void enviar(){
        nombre=tapa.getNombre();
        calle=tapa.getCalle();
        latitud=tapa.getLatitud();
        longitud=tapa.getLongitud();
        user_id=ApplicationConfig.getCurrentUser().getId_user();
        tapa_id="se inserta sola";
        descripcion=String.valueOf(descripcionTextView.getText());
        precio=String.valueOf(precioDesc.getText());
        puntuacion=String.valueOf(ratingBarDesc.getRating());
        Object[] DataTransfer = new Object[12];
        DataTransfer[0] = nombre;
        DataTransfer[1] = calle;
        DataTransfer[2] = latitud;
        DataTransfer[3] = longitud;
        DataTransfer[4] = user_id;
        DataTransfer[5] = descripcion;
        DataTransfer[6] = precio;
        DataTransfer[7] = puntuacion;
        DataTransfer[8] = img1;
        DataTransfer[9] = img2;
        DataTransfer[10] = img3;
        DataTransfer[11] = getContext();
        final AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setCancelable(true);
        dialog.setTitle("GRACIAS!");
        dialog.setMessage(String.valueOf("Gracias por colaborar! En cuanto tu publicación sea verificada, usted y toda la comunidad podrá visualizarla en el mapa. \n\n" +
                "Mientras tanto, puede consultar el estado de su publicación en el apartado de 'Mis publicaciones'."));
        dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                getActivity().onBackPressed();
            }
        });

        dialog.show();
        sendCommentAndDescription send= new sendCommentAndDescription();
        send.execute(DataTransfer);
    }




    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

    }



}
