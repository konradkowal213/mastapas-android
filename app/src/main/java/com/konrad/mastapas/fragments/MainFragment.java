package com.konrad.mastapas.fragments;


import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.konrad.mastapas.ApplicationConfig;
import com.konrad.mastapas.R;
import com.konrad.mastapas.adapters.listDescriptionsAdapter;
import com.konrad.mastapas.adapters.listMapAdapter;
import com.konrad.mastapas.clases.Descripcion;
import com.konrad.mastapas.clases.SortPlaces;
import com.konrad.mastapas.clases.Tapa;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static android.content.Context.LOCATION_SERVICE;

public class MainFragment extends Fragment implements OnMapReadyCallback,LocationListener,AdapterView.OnItemClickListener {

    GoogleMap map;
    ProgressDialog progressDialog;
    ArrayList<Tapa> arrayTapas = new ArrayList<Tapa>();
    ArrayList<Tapa> arrayTapasOrdenadas = new ArrayList<Tapa>();
    TextView verOpiniones;
    ArrayList<Descripcion> arrayDescripciones = new ArrayList<Descripcion>();
    LocationManager locManager;
    listMapAdapter adapter ;
    ListView listView ;
    private FragmentActivity myContext;
    ArrayList<Marker> markers=new ArrayList<>();
    private Dialog dialog;
    AlertDialog.Builder builder;





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstBundle) {
        View v = inflater.inflate(R.layout.fragment_main, container, false);
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 123);
        }
        listView = (ListView) getActivity().findViewById(R.id.linearMapList);
        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        progressDialog = new ProgressDialog(getActivity(), R.style.Theme_AppCompat_DayNight_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Descargando puntos......");
        progressDialog.show();
        locManager= (LocationManager)getContext().getSystemService(LOCATION_SERVICE);
        locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,1000L,500.0f, this);


        if((ApplicationConfig.comentariosAbiertos==true) && ApplicationConfig.descripcions!=null ){
            cargarDialogoComentarios();
        }


        return v;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            map.setMyLocationEnabled(true);
        }
        CameraPosition cameraPosition;
        if(ApplicationConfig.ultimoVisto!=null){
            cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(Double.valueOf(ApplicationConfig.ultimoVisto.latitude), Double.valueOf(ApplicationConfig.ultimoVisto.longitude)))      // Sets the center of the map to location user
                    .zoom(ApplicationConfig.lastZoom)                   // Sets the zoom
                    .build();                   // Creates a CameraPosition from the builder
        }else{
            cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(40.417044, -3.707702))      // Sets the center of the map to location user
                    .zoom(10 )                   // Sets the zoom
                    .build();                   // Creates a CameraPosition from the builder
        }

        map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition),1,null);
        if(ApplicationConfig.tapas.isEmpty()){
            getPoints(ApplicationConfig.getCurrentUser().getApi_Token(), googleMap);
        }else{
            for(int f=0;f<ApplicationConfig.tapas.size();f++){
                cargarAlert(ApplicationConfig.tapas.get(f));
            }
        }

    }


    public void getPoints(final String token, final GoogleMap map) {
        String url = ApplicationConfig.URL + "tapas";

        JsonArrayRequest jsonRequest = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                for (int f = 0; f < response.length(); f++) {
                    try {
                        Log.e("JSON", String.valueOf(response.getJSONObject(f)));
                        Tapa tapa = new Tapa(response.getJSONObject(f));
                        arrayTapas.add(tapa);
                        ApplicationConfig.tapas.add(tapa);
                        cargarAlert(tapa);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("JSON:", String.valueOf(error));
                progressDialog.dismiss();
                new AlertDialog.Builder(getActivity())
                        .setTitle("Error")
                        .setMessage("Error al cargar los datos, intentelo mas tarde")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/json");
                headers.put("Authorization", String.valueOf(token));
                return headers;
            }
        };
        Volley.newRequestQueue(getActivity()).add(jsonRequest);
    }

    private void getComments(Marker m) {
        String url = ApplicationConfig.URL + "description/" + String.valueOf(m.getTitle());
        arrayDescripciones.clear();

        JsonArrayRequest jsonRequest = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.e("e", String.valueOf(response));
                for (int f = 0; f < response.length(); f++) {
                    try {
                        Descripcion descripcion = new Descripcion(response.getJSONObject(f));
                        arrayDescripciones.add(descripcion);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                cargarDialogoComentarios();
                ApplicationConfig.descripcions=arrayDescripciones;
                progressDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/json");
                headers.put("Authorization", String.valueOf(ApplicationConfig.getCurrentUser().getApi_Token()));
                return headers;
            }
        };

        Volley.newRequestQueue(getActivity()).add(jsonRequest);

    }

    private void cargarAlert(Tapa tapa){
        LatLng point = new LatLng(Float.valueOf(tapa.getLatitud()), Float.valueOf(tapa.getLongitud()));
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(point);
        markerOptions.title(String.valueOf(tapa.getIdTapa()));
        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.pointer3));
        map.addMarker(markerOptions);
        map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                ApplicationConfig.ultimaTapa=marker.getPosition();
                ApplicationConfig.lastZoom=map.getCameraPosition().zoom;
                progressDialog.setMessage("Descargando comentarios....");
                progressDialog.show();
                marker.hideInfoWindow();
                LatLng coordinate = new LatLng(marker.getPosition().latitude, marker.getPosition().longitude); //Store these lat lng values somewhere. These should be constant.
                CameraUpdate location = CameraUpdateFactory.newLatLngZoom(coordinate, 15);
                map.animateCamera(location,1,null);
                getComments(marker);
                ApplicationConfig.m=marker.getTitle();
                return true;
            }
        });
        cargarAdapter();
    }

    private void cargarDialogoComentarios(){
        builder = new AlertDialog.Builder(getActivity());
        if(arrayDescripciones.isEmpty()){
            builder.setTitle(ApplicationConfig.descripcions.get(0).getTapa().getNombre());
        }else{
            builder.setTitle(arrayDescripciones.get(0).getTapa().getNombre());
        }
        final ListView modeList = new ListView(getActivity());
        if(arrayDescripciones.isEmpty()){
            listDescriptionsAdapter adapter = new listDescriptionsAdapter(getContext(), ApplicationConfig.descripcions);
            modeList.setAdapter(adapter);
        }else{
            listDescriptionsAdapter adapter = new listDescriptionsAdapter(getContext(), arrayDescripciones);
            modeList.setAdapter(adapter);

        }
        modeList.setClickable(true);

        modeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                DescriptionFragment fragment=new DescriptionFragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                Bundle bundle = new Bundle();
                bundle.putString("value_DESC", arrayDescripciones.get(i).getDescripcion());
                bundle.putString("value_PRICE", arrayDescripciones.get(i).getPrecio());
                bundle.putString("value_DATE", arrayDescripciones.get(i).getFecha_publicacion());
                bundle.putString("value_RAT", arrayDescripciones.get(i).getPuntuacion());
                bundle.putString("value_ID",arrayDescripciones.get(i).getID_Descripcion());
                fragment.setArguments(bundle);
                fragmentManager.beginTransaction()
                        .replace(R.id.contenido, fragment)
                        .commit();
                fragmentManager.executePendingTransactions();
                dialog.dismiss();
                ApplicationConfig.comentariosAbiertos=true;
            }
        });


        builder.setView(modeList);
        builder.setCancelable(false);
        builder.setPositiveButton("Volver al mapa", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        ApplicationConfig.comentariosAbiertos=false;

                    }
                });
        builder.setNegativeButton("Añadir Comentario", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                NewCommentFragment fragment=new NewCommentFragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                Bundle bundle = new Bundle();
                bundle.putString("value_ID", String.valueOf(ApplicationConfig.m));
                fragment.setArguments(bundle);
                fragmentManager.beginTransaction()
                        .replace(R.id.contenido, fragment)
                        .commit();
                fragmentManager.executePendingTransactions();
                dialog.dismiss();
                ApplicationConfig.comentariosAbiertos=true;

            }
        });
        dialog = builder.create();
        dialog.show();
    }

    private void cargarAdapter(){
        if(MainFragment.this.isVisible()){
            if(arrayTapas.isEmpty()){
                adapter = new listMapAdapter(getContext(), ApplicationConfig.tapas);
            }else{
                adapter = new listMapAdapter(getContext(), arrayTapas);
            }
            listView=(ListView) getActivity().findViewById(R.id.linearMapList);
            listView.setAdapter(adapter);
            listView.setOnItemClickListener(MainFragment.this);
            progressDialog.dismiss();
        }

    }

    public ArrayList ordenar(Location location) {
        double lat = location.getLatitude();
        double lng = location.getLongitude();
        LatLng latLng = new LatLng(lat, lng);

        //set up list
        ArrayList<Tapa> places = new ArrayList<Tapa>();

        if(arrayTapas.isEmpty()){
            places = ApplicationConfig.tapas;
            ApplicationConfig.tapas=places;
        }else{
            places = arrayTapas;
            arrayTapas=places;
        }


        //sort the list, give the Comparator the current location
        Collections.sort(places, new SortPlaces(latLng));

        return places;
    }


    @Override
    public void onLocationChanged(Location location) {
        if(MainFragment.this.isVisible()){
            if(ApplicationConfig.ultimaTapa==null){
                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(new LatLng(location.getLatitude(), location.getLongitude()))      // Sets the center of the map to location user
                        .zoom(15)                   // Sets the zoom
                        .build();                   // Creates a CameraPosition from the builder
                map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }
            arrayTapasOrdenadas=ordenar(location);
            arrayTapas=arrayTapasOrdenadas;
            if(listView!=null){
                listView.removeAllViewsInLayout();
                listView.setOnItemClickListener(this);
                adapter = new listMapAdapter(getContext(), arrayTapasOrdenadas);
                listView.setAdapter(adapter);
            }
        }
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, final int i, long l) {
        CameraPosition cameraPosition;
        if(arrayTapas.isEmpty()){
            cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(Double.valueOf(ApplicationConfig.tapas.get(i).getLatitud()), Double.valueOf(ApplicationConfig.tapas.get(i).getLongitud())))      // Sets the center of the map to location user
                    .zoom(15 )                   // Sets the zoom
                    .build();
        }else{
            cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(Double.valueOf(arrayTapas.get(i).getLatitud()), Double.valueOf(arrayTapas.get(i).getLongitud())))      // Sets the center of the map to location user
                    .zoom(15 )                   // Sets the zoom
                    .build();
        }
                        // Creates a CameraPosition from the builder
        map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        verOpiniones=(TextView) view.findViewById(R.id.directo);
        verOpiniones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LatLng latlong=new LatLng(Double.parseDouble(arrayTapas.get(i).getLatitud()),Double.parseDouble(arrayTapas.get(i).getLongitud()));
                ApplicationConfig.ultimaTapa=latlong;
                ApplicationConfig.m=String.valueOf(arrayTapas.get(i).getIdTapa());
                ApplicationConfig.lastZoom=map.getCameraPosition().zoom;
                progressDialog.setMessage("Descargando comentarios....");
                progressDialog.show();
                Marker punto = map.addMarker(new MarkerOptions()
                                    .position(new LatLng(37.7750, 122.4183))
                                    .title(arrayTapas.get(i).getIdTapa())
                                    .visible(false));
                getComments(punto);

            }
        });
    }

    @Override
    public void onPause() {
            LatLng location=new LatLng(map.getCameraPosition().target.latitude,map.getCameraPosition().target.longitude);
            ApplicationConfig.lastZoom=map.getCameraPosition().zoom;
            ApplicationConfig.ultimoVisto=location;

        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

}
