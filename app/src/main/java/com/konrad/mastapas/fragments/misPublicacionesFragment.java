package com.konrad.mastapas.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.konrad.mastapas.ApplicationConfig;
import com.konrad.mastapas.R;
import com.konrad.mastapas.adapters.listMyDescriptionsAdapter;
import com.konrad.mastapas.clases.Descripcion;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class misPublicacionesFragment extends Fragment implements AdapterView.OnItemClickListener{

    ProgressDialog progressDialog;
    ListView listView ;
    ArrayList<Descripcion> lista;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_mis_publicaciones, container, false);
        listView= (ListView) v.findViewById(R.id.listaMisAportaciones);
        progressDialog = new ProgressDialog(getActivity(), R.style.Theme_AppCompat_DayNight_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Descargando tus comentarios......");
        progressDialog.show();
        lista=new ArrayList<>();
        getComentarios(v);
        return v;
    }


    private void getComentarios(final View v){
        String url = ApplicationConfig.URL + "descriptionUser/"+String.valueOf(ApplicationConfig.getCurrentUser().getId_user());

        JsonArrayRequest jsonRequest = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                for (int f = 0; f < response.length(); f++) {
                    try {
                        Log.e("JSON", String.valueOf(response.getJSONObject(f)));
                        Descripcion d=new Descripcion(response.getJSONObject(f));
                        lista.add(d);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                if(lista.isEmpty()){
                    listView= (ListView) v.findViewById(R.id.listaMisAportaciones);
                    listView.setBackgroundResource(R.drawable.triste);
                }else{
                    listMyDescriptionsAdapter adapter = new listMyDescriptionsAdapter(getContext(), lista);
                    listView= (ListView) v.findViewById(R.id.listaMisAportaciones);
                    listView.setAdapter(adapter);
                    listView.setOnItemClickListener(misPublicacionesFragment.this);
                }


                progressDialog.dismiss();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("JSON:", String.valueOf(error));
                progressDialog.dismiss();
                new AlertDialog.Builder(getActivity())
                        .setTitle("Error")
                        .setMessage("Error al cargar los datos, intentelo mas tarde")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .show();

                progressDialog.dismiss();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/json");
                headers.put("Authorization", String.valueOf(ApplicationConfig.getCurrentUser().getApi_Token()));
                return headers;
            }
        };
        Volley.newRequestQueue(getActivity()).add(jsonRequest);
    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        ApplicationConfig.misOpiniones=true;
        DescriptionFragment fragment=new DescriptionFragment();
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        Bundle bundle = new Bundle();
        bundle.putString("value_DESC", lista.get(i).getDescripcion());
        bundle.putString("value_PRICE", lista.get(i).getPrecio());
        bundle.putString("value_DATE", lista.get(i).getFecha_publicacion());
        bundle.putString("value_RAT", lista.get(i).getPuntuacion());
        bundle.putString("value_ID",lista.get(i).getID_Descripcion());
        fragment.setArguments(bundle);
        fragmentManager.beginTransaction()
                .replace(R.id.contenido, fragment)
                .commit();

    }
}
