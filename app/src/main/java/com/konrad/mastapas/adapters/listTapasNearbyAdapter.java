package com.konrad.mastapas.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.konrad.mastapas.R;
import com.konrad.mastapas.clases.Tapa;

import java.util.ArrayList;

/**
 * Created by konra on 20/05/2017.
 */

public class listTapasNearbyAdapter extends ArrayAdapter<Tapa> {
    public listTapasNearbyAdapter(Context context, ArrayList<Tapa> tapas) {
        super(context, 0, tapas);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Tapa tapa = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.itemneatby, parent, false);
        }
        TextView nombre = (TextView) convertView.findViewById(R.id.nombre);
        TextView calle = (TextView) convertView.findViewById(R.id.calle);
        nombre.setText("Nombre: "+tapa.getNombre());
        calle.setText(tapa.getCalle());
        // Return the completed view to render on screen
        return convertView;
    }

}
