package com.konrad.mastapas.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import com.konrad.mastapas.R;
import com.konrad.mastapas.clases.Tapa;

import java.util.ArrayList;

/**
 * Created by konra on 20/05/2017.
 */

public class listMapAdapter extends ArrayAdapter<Tapa> {
    public listMapAdapter(Context context, ArrayList<Tapa> tapas) {
        super(context, 0, tapas);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Tapa tapa = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.itemlistcomentarioscopia, parent, false);
        }

        // Lookup view for data population
        TextView nombre = (TextView) convertView.findViewById(R.id.nombre);
        TextView tvPunct = (TextView) convertView.findViewById(R.id.punctuation);
        RatingBar puntos = (RatingBar) convertView.findViewById(R.id.ratingBar);

        TextView calle = (TextView) convertView.findViewById(R.id.calle);
        TextView opinionGeneral = (TextView) convertView.findViewById(R.id.categoria);


        puntos.setRating(Float.parseFloat(String.valueOf(tapa.getPuntuacionMedia())));
        // Populate the data into the template view using the data object
        Typeface font = Typeface.createFromAsset(getContext().getAssets(), "fonts/Blacksword.otf");
        tvPunct.setTypeface(font);
        tvPunct.setText(tapa.getPuntuacionMedia());
        nombre.setText(tapa.getNombre());
        calle.setText(tapa.getCalle());
        opinionGeneral.setText(tapa.getCategoria());
        // Return the completed view to render on screen
        return convertView;
    }


}
