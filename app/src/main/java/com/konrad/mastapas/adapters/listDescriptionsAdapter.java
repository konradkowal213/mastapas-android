package com.konrad.mastapas.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.konrad.mastapas.R;
import com.konrad.mastapas.clases.Descripcion;

import java.util.ArrayList;

/**
 * Created by konra on 20/05/2017.
 */

public class listDescriptionsAdapter extends ArrayAdapter<Descripcion> {
    public listDescriptionsAdapter(Context context, ArrayList<Descripcion> descripcions) {
        super(context, 0, descripcions);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Descripcion descripcion = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.itemlistlocales, parent, false);
        }
        ImageView img = (ImageView) convertView.findViewById(R.id.imagen);

        TextView desc = (TextView) convertView.findViewById(R.id.descripcion);


        TextView fecha = (TextView) convertView.findViewById(R.id.fecha);
        TextView precio = (TextView) convertView.findViewById(R.id.precio);

        String fechaSeparada[]=descripcion.getFecha_publicacion().split(" ");

        Double puntuacion=Double.valueOf(descripcion.getPuntuacion());
        if((puntuacion>=0) && (puntuacion<1)){
            img.setImageResource(R.mipmap.enfado);
        }else if((puntuacion>=1) && (puntuacion<2)){
            img.setImageResource(R.mipmap.mediomalo);
        }else if((puntuacion>=2) && (puntuacion<3)){
            img.setImageResource(R.mipmap.neutro);
        }else if((puntuacion>=3) && (puntuacion<4)){
            img.setImageResource(R.mipmap.bueno);
        }else if(puntuacion>=4){
            img.setImageResource(R.mipmap.excelente);
        }else {
            img.setImageResource(R.mipmap.noimage);
        }

        /*if(descripcion.getArrayImagenes()!=null) {
            String encodedDataString = String.valueOf(descripcion.getArrayImagenes().get(0).getBase64_img());
            encodedDataString = encodedDataString.replace("data:image/jpeg;base64,", "");

            byte[] imageAsBytes = Base64.decode(encodedDataString.getBytes(), 0);
            img.setImageBitmap(BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length));

        }else{
            img.setImageResource(R.mipmap.noimage);
        }*/

        fecha.setText(fechaSeparada[0]);
        precio.setText(descripcion.getPrecio()+" €");
        desc.setText(descripcion.getDescripcion());
        // Return the completed view to render on screen
        return convertView;
    }

}
