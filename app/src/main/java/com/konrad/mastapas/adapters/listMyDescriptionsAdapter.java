package com.konrad.mastapas.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import com.konrad.mastapas.R;
import com.konrad.mastapas.clases.Descripcion;

import java.util.ArrayList;

/**
 * Created by konra on 08/06/2017.
 */

public class listMyDescriptionsAdapter extends ArrayAdapter<Descripcion> {
    public listMyDescriptionsAdapter(Context context, ArrayList<Descripcion> descripcions) {
        super(context, 0, descripcions);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Descripcion desc = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.itemlistpublicaciones, parent, false);
        }

        // Lookup view for data population
        TextView aprobado = (TextView) convertView.findViewById(R.id.aprobado);
        TextView nombreBar = (TextView) convertView.findViewById(R.id.nombreBar);
        TextView fechapub = (TextView) convertView.findViewById(R.id.fechapub);
        TextView descripcion = (TextView) convertView.findViewById(R.id.descripcion);
        RatingBar ratingBar = (RatingBar) convertView.findViewById(R.id.ratingBar);
        TextView pagado = (TextView) convertView.findViewById(R.id.pagado);


        if(Integer.valueOf(desc.getAprobado())==1){
            aprobado.setBackgroundResource(R.color.colorGreen );
        }else if( Integer.valueOf(desc.getAprobado())==0 ){
            aprobado.setBackgroundResource(R.color.colorRed );
        }else {
            aprobado.setBackgroundResource(R.color.colorGreyForce);
        }
        nombreBar.setText(desc.getTapa().getNombre());
        String fechaSeparada[]=desc.getFecha_publicacion().split(" ");
        fechapub.setText(fechaSeparada[0]);
        descripcion.setText(desc.getDescripcion());
        ratingBar.setRating(Float.parseFloat(String.valueOf(desc.getPuntuacion())));
        pagado.setText("Pagado: "+ desc.getPrecio() + " €");
        // Return the completed view to render on screen
        return convertView;
    }
}
