package com.konrad.mastapas;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.konrad.mastapas.clases.Tapa;
import com.konrad.mastapas.clases.User;
import com.konrad.mastapas.dbHelper.UserDbHelper;
import com.konrad.mastapas.fragments.MainFragment;
import com.konrad.mastapas.fragments.NewLocalAndCommentFragment;
import com.konrad.mastapas.fragments.misPublicacionesFragment;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.konrad.mastapas.fragments.NewLocalAndCommentFragment.autocompleteFragment;

public class ActivityPrincipal extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{
    ProgressDialog progressDialog;
    Fragment fragmentMain= new MainFragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        getSupportFragmentManager().beginTransaction().replace(R.id.contenido,fragmentMain).commit();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        progressDialog = new ProgressDialog(ActivityPrincipal.this, R.style.Theme_AppCompat_DayNight_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Cambiando contraseña");
    }

    @Override
    public void onBackPressed() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {//si barra lateraal abierta
            drawer.closeDrawer(GravityCompat.START);
        } else if(ApplicationConfig.misOpiniones==true){
            Fragment misOpiniones=new misPublicacionesFragment();
            replaceFragment(misOpiniones);
            ApplicationConfig.misOpiniones=false;
        } else if(fragmentMain.isVisible()==false) {
            replaceFragment(fragmentMain);
        }else {
            createCloseDialog();
        }


    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        Fragment fragment=null;

        if (id == R.id.navmap) {
            replaceFragment(fragmentMain);
        } else if (id == R.id.nav_gallery) {
            createCloseSessionDialog();
        } else if (id == R.id.nav_slideshow) {
            if(autocompleteFragment!=null ){
                android.app.FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.remove(autocompleteFragment);
                ft.commit();
            }
            fragment= new NewLocalAndCommentFragment();
            replaceFragment(fragment);
        } else if (id == R.id.nav_share) {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_SUBJECT, "");
            intent.putExtra(Intent.EXTRA_TEXT, "La mejor aplicación de tapas por Madrid! Descárgala aquí! https://play.google.com/apps/testing/com.konrad.mastapas");
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            this.startActivity(Intent.createChooser(intent,  "Compartir en" ));
        } else if (id == R.id.nav_send) {
            Uri uri = Uri.parse("https://www.paypal.me/konrad213");
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);
        } else if (id == R.id.nav_pass){
            createLoginDialogo();
        } else if (id == R.id.sugerencia ){
            Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto","konrad213@hotmail.com", null));
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "MasTapas");
            startActivity(Intent.createChooser(emailIntent,  "Enviar con:"));
        }else if (id==R.id.nav_publications){
            fragment= new misPublicacionesFragment();
            replaceFragment(fragment);
        }



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return false;
    }

    public AlertDialog createCloseDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);

        LayoutInflater inflater = this.getLayoutInflater();

        View v = inflater.inflate(R.layout.fragment_closeapplication, null);

        builder.setView(v);

        final TextView cancelar = (TextView) v.findViewById(R.id.cancel);
        final TextView aceptar = (TextView) v.findViewById(R.id.cerrarApp);

        final AlertDialog d = builder.show();

        aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                android.os.Process.killProcess(android.os.Process.myPid());
            }
        });

        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                d.dismiss();
            }
        });
        builder.setCancelable(false);

        return builder.create();
    }

    public AlertDialog createCloseSessionDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);

        LayoutInflater inflater = this.getLayoutInflater();

        View v = inflater.inflate(R.layout.fragment_closesession, null);

        builder.setView(v);

        final TextView cancelar = (TextView) v.findViewById(R.id.cancelSesion);
        final TextView aceptar = (TextView) v.findViewById(R.id.cerrarSesion);

        final AlertDialog d = builder.show();

        aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UserDbHelper db=new UserDbHelper(ActivityPrincipal.this);
                ApplicationConfig.comentariosAbiertos=false;
                ApplicationConfig.tapas=null;
                ApplicationConfig.tapas=new ArrayList<Tapa>();
                ApplicationConfig.descripcions=null;
                db.deleteUser(ApplicationConfig.getCurrentUser().getId_user());
                User user=new User();
                ApplicationConfig.setCurrentUser(user);
                Intent newAccount = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(newAccount);
                finish();
            }
        });

        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                d.dismiss();
            }
        });
        builder.setCancelable(false);

        return builder.create();
    }

    public AlertDialog createLoginDialogo() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);

        LayoutInflater inflater = this.getLayoutInflater();

        View v = inflater.inflate(R.layout.fragment_new_password, null);

        builder.setView(v);

        final TextView oldpass = (TextView) v.findViewById(R.id.oldPass);
        final TextView pass1 = (TextView) v.findViewById(R.id.newPass1);
        final TextView pass2 = (TextView) v.findViewById(R.id.newPass2);

        TextView accept = (TextView) v.findViewById(R.id.accept) ;
        TextView closeDialog = (TextView) v.findViewById(R.id.close);
        final AlertDialog d = builder.show();

        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validar(oldpass, pass1, pass2)==true){
                    changePass(ApplicationConfig.getCurrentUser().getMail(),pass1.getText().toString());
                }


            }
        });

        closeDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                d.dismiss();
            }
        });
        builder.setCancelable(false);

        return builder.create();
    }

    private boolean validar(TextView oldpass, TextView pass1, TextView pass2){
        if(oldpass.getText().toString().equals(ApplicationConfig.getCurrentUser().getPass())){

        }else{
            oldpass.setError("La contraseña actual no coincide con la introducida");
            return false;
        }
        if((pass1.length()<6)){
            pass1.setError("Debe tener al menos 6 letras");
            return false;
        }
        if(pass1.getText().toString().equals(pass2.getText().toString())){
        }else{
            pass2.setError("Deben coincidir las dos contraseñas");
            return false;
        }
        return true;
    }

    private void changePass(String correo, String password){

            Map<String, String> params = new HashMap();

            params.put("correo", String.valueOf(correo));
            params.put("password", String.valueOf(password));
            JSONObject parameters = null;

            parameters = new JSONObject(params);


            String url = ApplicationConfig.URL + "changePassword";

            JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    progressDialog.dismiss();
                    new AlertDialog.Builder(ActivityPrincipal.this)
                            .setTitle("Éxito!")
                            .setMessage("Tu contraseña ha sido cambiada, por motivos de seguridad, cerraremos su sesion actual")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    UserDbHelper db=new UserDbHelper(ActivityPrincipal.this);
                                    ApplicationConfig.comentariosAbiertos=false;
                                    ApplicationConfig.tapas=null;
                                    ApplicationConfig.tapas=new ArrayList<Tapa>();
                                    ApplicationConfig.descripcions=null;
                                    db.deleteUser(ApplicationConfig.getCurrentUser().getId_user());
                                    User user=new User();
                                    ApplicationConfig.setCurrentUser(user);
                                    Intent newAccount = new Intent(getApplicationContext(), LoginActivity.class);
                                    startActivity(newAccount);
                                    finish();
                                }
                            })
                            .show();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                    //501 ya existe nick o usuario ,500 error interno del servidor
                    if (error.networkResponse.statusCode == 501) {
                        new AlertDialog.Builder(ActivityPrincipal.this)
                                .setTitle("Ups!")
                                .setCancelable(false)
                                .setMessage("Ha habido un problema cambiando la contraseña")
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                })
                                .show();
                    } else if (error.networkResponse.statusCode == 500) {
                        new AlertDialog.Builder(ActivityPrincipal.this)
                                .setTitle("Ups!")
                                .setCancelable(false)
                                .setMessage("Error interno del servidor, intentelo mas tarde")
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                })
                                .show();
                    }else{
                        new AlertDialog.Builder(ActivityPrincipal.this)
                                .setTitle("Ups!")
                                .setCancelable(false)
                                .setMessage("Error interno del servidor, intentelo mas tarde")
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                })
                                .show();
                    }
                }
            }){
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    //headers.put("Content-Type", "application/json");
                    headers.put("Authorization", String.valueOf(ApplicationConfig.getCurrentUser().getApi_Token()));
                    return headers;
                }
            };

            Volley.newRequestQueue(this).add(jsonRequest);


    }

    private void replaceFragment (Fragment fragment){

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        ft.replace(R.id.contenido, fragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.commit();
    }
}
